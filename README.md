# README #

Mobile Automation Practice Problem

### Description ###
Small framework to resolve Applause Automation Practice Problem

### Dependencies ###
 
* Appium for Android testing.
* Selenium (required by Appium)
* Cucumber for BDD
* JUnit for Assertions
* Log4j for logging
* OpenCSV to read devices.csv file (holds each device configuration)

### How do I get set up? ###

* git clone https://alexis_giovoglanian@bitbucket.org/alexis_giovoglanian/applause-framework.git
* import maven dependencies
* add your device to /src/test/resources/devices.csv
* start an appium server
* make sure you set the ip and port correctly on the devices.csv file
* run the class /src/test/runners/WholeFoodsRunner with the following vm arguments: -Ddevice=(name of the device in devices.csv file - first column)

### Who do I talk to? ###

* alexis.giovoglanian@gmail.com
* skype: alesi.metal