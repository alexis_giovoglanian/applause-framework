@WholeFoods
Feature: Whole Foods Market Android Recipe Search

  As a user I want to search for Recipe

@NonLoggedIn
  Scenario Outline: Recipe basic search - Non logged in
    Given I open the Whole Foods Market Android App
    When I search for a <recipe>
    Then I verify there are results displayed

  Examples:
  | recipe      |
  | pasta       |
  | hamburguers |