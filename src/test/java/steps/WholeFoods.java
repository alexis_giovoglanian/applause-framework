package steps;

import com.applause.framework.functions.Functions;
import com.applause.framework.logger.Log;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by Alexis on 22/03/2016.
 */
public class WholeFoods extends Functions implements WholeFoodsElementsInterface{
    Log logger = new Log();
    String recipe;

    @Given("^I open the Whole Foods Market Android App$")
    public void I_open_the_Whole_Foods_Market_Android_App() throws Throwable {
        openApp(wholeFoodPath);
        //Sometimes a popup is displayed on the App. This will check if it's displayed and close as many of them
        //as needed (Up to 5, maximum encountered: 2
        waitForElementByIDToBeDisplayed(popupCloseButton);
        setWaitTime(5);
        int tries = 0;
        while (waitForElementByIDToBeDisplayed(popupCloseButton) && tries <= 3) {
            findElementByID(popupCloseButton);
            click();
            tries++;
        }
        setWaitTime(20);
    }

    @When("^I search for a (.*)$")
    public void I_search_for_a_recipe(String recipe) throws Throwable {
        this.recipe = recipe;
        waitForElementByIDToBeDisplayed(recipesNonAuthenticated);
        findElementByID(recipesNonAuthenticated);
        click();
        waitForElementByIDToBeDisplayed(searchField);
        findElementByID(searchField);
        type(recipe);
        findElementByID(searchButton);
        click();
    }

    @Then("^I verify there are results displayed$")
    public void I_verify_there_are_results_displayed() throws Throwable {
        waitForElementByIDToBeDisplayed(resultsNumber);
        findElementByID(resultsNumber);
        if(parseIntFromString(getText()) > 0) {
            logger.log("Recipes found with keyword '" + this.recipe + "': " + getText());
        }
        else
            logger.fail("No Recipes where found with keyword: "+ this.recipe);
    }
}
