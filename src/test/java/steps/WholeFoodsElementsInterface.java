package steps;

/**
 * Created by Alexis on 22/03/2016.
 */
public interface WholeFoodsElementsInterface {
    //App Location
    String wholeFoodPath = System.getProperty("user.dir")+"\\src\\test\\resources\\App\\wmf.apk";

    //Android Elements
    String popupCloseButton = "com.wholefoods.wholefoodsmarket:id/close_button";//ID
    String recipesNonAuthenticated = "com.wholefoods.wholefoodsmarket:id/tvUnauthenticatedRecipe";//ID
    String searchField = "com.wholefoods.wholefoodsmarket:id/etHomeSearch";//ID
    String searchButton = "com.wholefoods.wholefoodsmarket:id/imgSearch";//ID
    String resultsNumber = "com.wholefoods.wholefoodsmarket:id/searchResultsNumber";//ID
}
