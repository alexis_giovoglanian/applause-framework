package steps.setup;

import com.applause.framework.functions.Functions;
import com.applause.framework.logger.Log;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Alexis on 23/03/2016.
 */
public class Setup extends Functions{
    Log log = new Log();
    static final String currentDate = (new SimpleDateFormat("yyyyMMdd")).format(Calendar.getInstance().getTime());
    static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HHmmss");
    static final String currentTime = SIMPLE_DATE_FORMAT.format(Calendar.getInstance().getTime());

    @Before
    public void before(Scenario scenario){
        log.log("Starting scenario execution: " + scenario.getName());
    }

    @After
    public void after(Scenario scenario){
        if(scenario.isFailed()){
            screenshot(currentDate+"//"+scenario.getName()+"-"+currentTime);
        }
        close();
        log.log("Finished scenario execution: " + scenario.getName());
        log.log("Execution result: " + scenario.getStatus());
    }
}
