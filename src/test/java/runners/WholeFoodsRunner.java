package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Alexis on 23/03/2016.
 */
@CucumberOptions(
        plugin = {"pretty",
                "html:target/cucumber/cucumber-html-report",
                "json:target/cucumber/cucumber-json-report.json"
        },
        monochrome=true,
        tags = {"@WholeFoods"},
        dryRun = false,
        features = "src/test/resources/features",
        glue = {"steps"})

@RunWith(Cucumber.class)
public class WholeFoodsRunner {
}
