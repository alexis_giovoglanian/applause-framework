package com.applause.framework.utilities;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alexis on 22/03/2016.
 */
public class CsvHandler {
    protected static String CSVDevices = "src/test/resources/devices.csv";
    //Build reader instance
    static CSVReader reader = null;

    /**
     * Reads the devices.csv file
     * @return allRows as List<String[]>
     * @throws Exception
     */
    public static List<String[]> deviceCSVReader() throws Exception{
        List<String[]> allRows = null;

        reader = new CSVReader(new FileReader(CSVDevices), ',', '"', 0);

        //Read all rows at once
        try {
            allRows = reader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allRows;
    }
}
