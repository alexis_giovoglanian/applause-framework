package com.applause.framework.functions;

import com.applause.framework.driverfactory.DriverFactory;
import com.applause.framework.logger.Log;
import io.appium.java_client.AppiumDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Created by Alexis on 22/03/2016.
 */
public class Functions extends DriverFactory{
    Log logger = new Log();
    static AppiumDriver driver;
    WebElement currentElement;
    int defaultWaitTime = 20;

    /**
     * Start an Android App and sets the driver
     * @param app (path to apk file)
     * @Return true if the App is opened, false if not
     */
    public Boolean openApp(String app){
        if(this.driver==null){
            File appPath = new File(app);
            if(appPath.exists()) {
                setApp(app);
            }else {
                logger.fail("App not found in path: " + app);
            }
            try {
                this.driver = getDriver();
                return true;
            }catch (Exception e){
                logger.fail("Unable to open the App" + e);
                return false;
            }
        }else{
            logger.fail("Driver already started. Close app first");
            return false;
        }
    }

    /**
     * Close The driver if exists
     * @Return true if the driver is closed, false if not
     */
    public Boolean close() {
            try {
                this.driver.quit();
                this.driver = null;
                return true;
            } catch (Exception e) {
                logger.error("Driver already closed" + e);
                return false;
            }
        }

    /**************
        Locators
     **************/

    /**
     * Sets current element to given element by id
     * @param id
     * @return true if found, false if not
     */
    public Boolean findElementByID(String id){
        try{
            this.currentElement = this.driver.findElement(By.id(id));
            return true;
        }catch (NoSuchElementException e){
            logger.error("Unable to find element with id: " + id);
            return false;
        }
    }

    /**************
         Wait Methods
     **************/

    /**
     * Sets the wait time for all the Wait methods, like waitForElementByIDToBeDisplayed(String id)
     * @param time
     */
    public void setWaitTime(int time){
        this.defaultWaitTime = time;
    }

    /**
     * Waits until element with given id is displayed
     * @param id
     * @return true if found within defaultWaitTime, false if not
     */
    public Boolean waitForElementByIDToBeDisplayed(String id){
        try {
            WebDriverWait wait = new WebDriverWait(this.driver, this.defaultWaitTime);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
            return true;
        }catch (Exception e){
            logger.error("Unable to find element with id: " + id);
            return false;
        }
    }

    /**************
     WebElement interaction
     **************/

    /**
     * Clicks on current element
     * @return true if able to click on currentElement, false if not
     */
    public Boolean click(){
        try{
            this.currentElement.click();
            return true;
        }catch (NoSuchElementException e){
            logger.error("Unable to click on current element: " + e);
            return false;
        }
    }

    /**
     * Types text on current element
     * @param text
     * @return true if able to type on current element, false if not
     */
    public Boolean type(String text){
        try{
            this.currentElement.sendKeys(text);
            return true;
        }catch (Exception e){
            logger.error("Unable to type text on current element." + e);
            return false;
        }
    }

    /**
     * Gets text from current element
     * @return current element's text
     */
    public String getText() {
        return this.currentElement.getText().trim();
    }

    /**************
        Misc
     **************/

    /**
     * Transforms string to int
     * @param value
     * @return int parsed from given string
     */
    public int parseIntFromString(String value){
        return Integer.parseInt(value);
    }

    /**
     * Takes a screenshot and stores with current format: "(WorkingDirectory)\target\output\screenshots\fileName.jpg"
     * @param fileName
     * @return true if screenshot is taken and stored, false if not
     */
    public Boolean screenshot(String fileName){
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            File path = new File(System.getProperty("user.dir") + "\\target\\output\\screenshots\\" + fileName + ".jpg");
            FileUtils.copyFile(scrFile, path);
            logger.log("Screenshot saved in: " + path.toString());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
