package com.applause.framework.logger;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;

/**
 * Created by Alexis on 22/03/2016.
 */
public class Log {
    private Logger logger = LogManager.getLogger(Log.class.getName());

    public void log(String log){
        logger.info(log);
    }

    public void debug(String log){
        logger.debug(log);
    }

    public void error(String log) {logger.error(log);}

    public void fail(String log){
        logger.error(log);
        Assert.fail();
    }
}
