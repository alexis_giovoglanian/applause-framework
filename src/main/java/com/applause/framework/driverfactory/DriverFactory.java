package com.applause.framework.driverfactory;

import com.applause.framework.logger.Log;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import java.net.URL;

/**
 * Created by Alexis on 22/03/2016.
 */
public class DriverFactory {
    Device device = new Device();
    Log logger = new Log();
    AppiumDriver driver;
    String app;

    /**
     * Sets app.
     * This is passed to the Device class through setDriver
     * @param app
     */
    public void setApp(String app){
        this.app = app;
    }

    /**
     * Sets the Android driver
     */
    private void setDriver(){
        if(this.app!=null){
            device.setApp(this.app);
        }
        if(this.driver==null){
            try {
                URL url = new URL("http://localhost:4723/wd/hub");
                logger.log("Starting Device");
                logger.debug("-----------------Device Capabilities:");
                for(String cap:device.getCapabilities().asMap().keySet()){
                    logger.log("['"+cap+"':'"+String.valueOf(device.getCapabilities().asMap().get(cap))+"']");
                }
                this.driver = new AndroidDriver(url, device.getCapabilities());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * public call to setDriver()
     * @return driver
     */
    public AppiumDriver getDriver(){
        setDriver();
        return this.driver;
    }

}


