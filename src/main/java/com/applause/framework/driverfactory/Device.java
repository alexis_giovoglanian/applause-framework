package com.applause.framework.driverfactory;

import com.applause.framework.logger.Log;
import com.applause.framework.utilities.CsvHandler;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.List;
import java.util.Objects;

/**
 * Created by Alexis on 22/03/2016.
 */
public class Device {
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    Log logger = new Log();
    String device = "";
    String platformName = "";
    String platformVersion = "";
    String deviceName = "";
    String appiumServerIP = "";
    String appiumServerPort = "";
    String app = "";

    /**
     * Sets the app parameter to be used as a Desired Capability
     * @param app
     */
    public void setApp(String app){
        this.app = app;
    }

    /**
     * Sets Desired Capabilities.
     * Reads the values from devices.csv file
     * @throws Exception
     */
    private void setCapabilities() throws Exception {
        String deviceUnderTest = System.getProperty("device");
        String[] deviceColumn = new String[0];

        if(deviceUnderTest==null){
            logger.error("Missing runtime argument -Ddevice");
            throw new NullPointerException();
        }
        try{
            List<String[]> table = CsvHandler.deviceCSVReader();
            String[] columnHeaders = table.get(0);
            for(String[] devices : table){
                if(devices[0].trim().equals(deviceUnderTest)){
                    deviceColumn = devices;
                    break;
                }
            }
            for (int i = 0; i < columnHeaders.length; i++){
                if(columnHeaders[i].trim().equalsIgnoreCase("DEVICE")){
                    this.device = deviceColumn[i];
                }
                if(columnHeaders[i].trim().equalsIgnoreCase("platformName")){
                    this.platformName = deviceColumn[i];
                }
                if(columnHeaders[i].trim().equalsIgnoreCase("platformVersion")){
                    this.platformVersion = deviceColumn[i];
                }
                if(columnHeaders[i].trim().equalsIgnoreCase("deviceName")){
                    this.deviceName = deviceColumn[i];
                }
                if(columnHeaders[i].trim().equalsIgnoreCase("appiumServerIP")){
                    this.appiumServerIP = deviceColumn[i];
                }
                if(columnHeaders[i].trim().equalsIgnoreCase("appiumServerPort")){
                    this.appiumServerPort = deviceColumn[i];
                }
            }
        }catch (Exception e){
            logger.error(("Missing CSV File - check src/test/resources/devices.csv"));
            throw e;
        }
        if(deviceColumn.length == 0){
            logger.error("The specified device: "+deviceUnderTest+" is not defined in the file devices.csv");
            throw new NullPointerException();
        }
        this.capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, this.deviceName);
        this.capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, this.platformName);
        this.capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, this.platformVersion);
        if(this.app.length() > 0){
                this.capabilities.setCapability(MobileCapabilityType.APP, this.app);
            }
        }

    /**
     * Sets the capabilities
     * @return Desired Capabilities
     * @throws Exception
     */
    public DesiredCapabilities getCapabilities() throws Exception {
        setCapabilities();
        return this.capabilities;
    }

}
